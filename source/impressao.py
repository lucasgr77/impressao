import glob
import os
import time
import logging
from dotenv import load_dotenv
logging.basicConfig(filename='impressao_logs.log', level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(funcName)s => %(message)s')



def get_arquivos_text(path):
    lista_arquivos = []
    for arquivo in glob.glob(path):
        lista_arquivos.append(arquivo)
    return lista_arquivos


def deleta_arquivo(path):
    os.remove(path)


def imprimir(arquivo):
    nome_impressora = os.getenv('NOME_IMPRESSORA')
    win32api.ShellExecute(
        0,
        'print',
        arquivo,
        nome_impressora,
        ".",
        0
    )


def controle_impressao():
    path = os.getenv('DIRETORIO_ARQUIVOS')
    tempo_ocioso = os.getenv('TEMPO_ESPERA_SEGUNDOS')
    if tempo_ocioso is None or tempo_ocioso == '':
        tempo_ocioso = 10
    tempo_ocioso = int(tempo_ocioso)
    try:
        while(True):
            lista_arquivos = get_arquivos_text(path)
            for arquivo in lista_arquivos:
                imprimir(arquivo)
                deleta_arquivo(arquivo)
            time.sleep(tempo_ocioso)
    except Exception as e:
        logging.debug(e)


def inicializador():
    load_dotenv()
    controle_impressao()

inicializador()